<?php


/* Template Name: commonpage*/


get_header(); ?>
<?php  $image_url= wp_get_attachment_url( get_post_thumbnail_id() );?>
<?php if( !empty(get_the_post_thumbnail()) ) { ?>

<section class="page_banner" style="background-image:url(<?php echo $image_url;?>">
  <div class="page_title mt-5 pt-5">
     <div class="default_title wow fadeIn">
       <h2><?php echo the_title();?></h2>
     </div>
  </div>
</section>

<?php } else { ?>
<section class="page_banner" style="background-image:url(<?php echo get_template_directory_uri();?>/images/investors_page_banner.jpg">
  <div class="page_title mt-5 pt-5">
    <div class="default_title wow fadeIn">
       <h2><?php echo the_title();?></h2>
    </div>
  </div>
</section>
<?php } ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

 <section class="default_sec">
   <?php the_content(__('(more...)')); ?>




     <?php endwhile; else: ?>
      
       <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

     <?php endif; ?>
 </section>

 
<?php get_footer();?>