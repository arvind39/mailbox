 <?php

  /* Template Name: About Us*/



  get_header(); ?>



 <?php



  //About Us

  $about = get_post(120);

  $about_title = $about->post_title;

  $about_content = $about->post_content;



  //About Team

  $about_emp1 = get_post(121);

  $about_emp1_title = $about_emp1->post_title;

  $about_emp1_content = $about_emp1->post_content;

  $about_emp1_img = wp_get_attachment_url(get_post_thumbnail_id($about_emp1->ID));



  $about_emp2 = get_post(123);

  $about_emp2_title = $about_emp2->post_title;

  $about_emp2_content = $about_emp2->post_content;

  $about_emp2_img = wp_get_attachment_url(get_post_thumbnail_id($about_emp2->ID));



  $about_emp3 = get_post(245);

  $about_emp3_title = $about_emp3->post_title;

  $about_emp3_content = $about_emp3->post_content;

  $about_emp3_img = wp_get_attachment_url(get_post_thumbnail_id($about_emp3->ID));



  $about_emp4 = get_post(248);

  $about_emp4_title = $about_emp4->post_title;

  $about_emp4_content = $about_emp4->post_content;

  $about_emp4_img = wp_get_attachment_url(get_post_thumbnail_id($about_emp4->ID));



  ?>



 <?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>

 <?php if (!empty(get_the_post_thumbnail())) { ?>



   <section class="page_banner" style="background-image:url(<?php echo $image_url; ?>">

     <div class="page_title mt-lg-5 mt-md-3 pt-5">

       <div class="default_title wow fadeIn">

         <h2><?php echo the_title(); ?></h2>

       </div>

     </div>

   </section>

 <?php } else { ?>

   <section class="page_banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/about_page_banner.jpg">

     <div class="page_title mt-lg-5 mt-md-3 pt-5">

       <div class="default_title wow fadeIn">

         <h2><?php echo the_title(); ?></h2>

       </div>

     </div>

   </section>

 <?php } ?>

 <section class="about_sec1 bg_light_gray">

   <div class="container">

     <div class="row">

       <div class="col-md-12">

         <div class="about_us">

           <div class="row">



             <div class="col-md-12 col-lg-12 wow fadeIn">

               <div class="default_title title_3 wow fadeIn">

                 <h2><?php echo $about_title ?></h2>

               </div>

               <?php echo $about_content ?>



             </div>

           </div>

         </div>

       </div>

       <div class="col-md-12 col-lg-12 wow fadeIn">

         <div class="default_title title_3 pt-5 wow fadeIn">

           <h2>How We Execute</h2>

         </div>

         <p>Not only do we know how to make spaces people want to live in, we are passionate about investing in them.</p>

         <p>As a firm that vertically integrates development, construction, property management, and asset management we are able to add value efficiently and effectively. Acutely aware of the monetary impact each decision makes, we unlock potential with simple and surprising solutions.</p>

         <!-- <a href="http://localhost/mailboxmoney/contact/" class="link_btn mt-4">

           <span>Contact us</span>

           <svg width="13px" height="10px" viewBox="0 0 13 10">

             <path d="M1,5 L11,5"></path>

             <polyline points="8 1 12 5 8 9"></polyline>

           </svg>

         </a> -->

       </div>

     </div>

   </div>

 </section>



 <section class="about_sec2">

   <div class="container">

     <div class="row">

       <div class="col-md-12">

         <div class="default_title text-center">

           <h2>Our Team</h2>

         </div>

         <div class="about_team mt-5 ltr">

           <div class="row">

             <div class="col-md-5">

               <div class="about_img wow fadeIn">

                 <img src="<?php echo $about_emp1_img ?>" class="img-fluid">

               </div>

             </div>

             <div class="col-md-7">

               <div class="about_content ps-lg-4 mt-md-0 mt-4 wow fadeIn">

                 <h2 class="mb-4"><?php echo $about_emp1_title ?></h2>

                 <?php echo $about_emp1_content ?>

               </div>

             </div>

           </div>

         </div>

         <div class="about_team mt-5 mt-xl-0 rtl">

           <div class="row align-items-lg-end flex-sm-row flex-column-reverse">



             <div class="col-md-7">

               <div class="about_content pe-lg-4 text-md-end mt-md-0 mt-4 wow fadeIn">

                 <h2 class="mb-4"><?php echo $about_emp4_title ?></h2>

                 <?php echo $about_emp4_content ?>

               </div>

             </div>

             <div class="col-md-5">

               <div class="about_img wow fadeIn">

                 <img src="<?php echo $about_emp4_img ?>" class="img-fluid">

               </div>

             </div>

           </div>

         </div>

         <div class="about_team mt-5 ltr">

           <div class="row">

             <div class="col-md-5">

               <div class="about_img wow fadeIn">

                 <img src="<?php echo $about_emp3_img ?>" class="img-fluid">

               </div>

             </div>

             <div class="col-md-7 d-flex align-items-center">

               <div class="about_content ps-lg-4 mt-md-0 mt-4 wow fadeIn">

                 <h2 class="mb-4"><?php echo $about_emp3_title ?></h2>

                 <?php echo $about_emp3_content ?>

               </div>

             </div>

           </div>

         </div>

         <div class="about_team mt-5 mt-xl-0 rtl">

           <div class="row align-items-lg-center flex-sm-row flex-column-reverse">



             <div class="col-md-7">

               <div class="about_content pe-lg-4 text-md-end mt-md-0 mt-4 wow fadeIn">

                 <h2 class="mb-4"><?php echo $about_emp2_title ?></h2>

                 <?php echo $about_emp2_content ?>

               </div>

             </div>

             <div class="col-md-5">

               <div class="about_img wow fadeIn">

                 <img src="<?php echo $about_emp2_img ?>" class="img-fluid">

               </div>

             </div>

           </div>

         </div>





       </div>

     </div>

   </div>

 </section>

 <section class="about_sec3">

   <div class="container">

     <div class="row">

       <div class="col-md-12>">

         <div class="default_title text-center">

           <h2>Our Partners</h2>

         </div>

         <div class="team_part pt-5">

           <div class="row">

             <?php

              $args = array('post_type' => 'about_us', 'category_name' => 'About Team', 'posts_per_page' => 3, 'order' => 'ASC');

              $the_query = new WP_Query($args);

              ?>

             <?php if ($the_query->have_posts()) : ?>

               <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                 <div class="col-md-4 mb-md-0 mb-4 wow fadeIn">

                   <div class="team_box d-flex align-items-center justify-content-center p-3">

                     <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">

                   </div>

                 </div>

               <?php endwhile;

                wp_reset_postdata(); ?>

             <?php else :  ?>

               <p><?php echo ('Sorry, no posts matched your criteria.'); ?></p>

             <?php endif; ?>

             <!-- <div class="col-md-4 mb-md-0 mb-4 wow fadeIn">

                <div class="team_box d-flex align-items-center justify-content-center p-3">

                  <img src="images/t2.png" class="img-fluid">

                </div>

              </div>

              <div class="col-md-4 mb-md-0 mb-4 wow fadeIn">

                <div class="team_box d-flex align-items-center justify-content-center p-3">

                  <img src="images/t3.png" class="img-fluid">

                </div>

              </div> -->

           </div>

         </div>

       </div>

     </div>

   </div>

 </section>

 <section class="ps-timeline-sec">

   <div class="container">

     <div class="row">

       <div class="col-md-12">

         <div class="default_title wow fadeIn mt-5 mb-5 text-center">

           <h2>How we started</h2>

         </div>
         <p class="scroll_btn">
             <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="500px" height="500px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
               <path fill-rule="evenodd" clip-rule="evenodd" d="M124.64,483.51c-2.689-0.966-4.521-2.889-6.168-5.162
                c-16.754-23.126-27.591-48.713-31.566-77.019c-1.252-8.917-1.951-17.985-1.986-26.988c-0.19-48.472-0.102-96.945-0.083-145.417
                c0.005-13.217,8.024-23.271,20.605-25.76c4.123-0.816,8.244-0.471,12.26,0.923c12.516,4.349,23.123,11.528,31.916,21.417
                c0.475,0.533,0.96,1.059,1.44,1.587c0.212-0.033,0.423-0.064,0.635-0.097c0-47.807,0-95.613,0-143.583c-17.772,0-36.033,0-54.681,0
                c0.636,0.674,1.134,1.231,1.662,1.76c5.657,5.664,11.34,11.301,16.967,16.994c2.756,2.79,3.369,6.289,1.842,9.592
                c-1.469,3.18-4.714,5.262-8.28,4.734c-1.793-0.265-3.844-1.066-5.105-2.308C91.058,101.337,78.154,88.351,65.215,75.4
                c-0.101-0.101-0.157-0.249-0.297-0.476c0.292-0.36,0.575-0.781,0.927-1.134C78.468,61.153,91.1,48.523,103.726,35.889
                c2.464-2.466,5.379-3.397,8.725-2.325c3.242,1.039,5.176,3.383,5.75,6.724c0.502,2.921-0.563,5.371-2.626,7.433
                c-5.505,5.5-11,11.011-16.517,16.5c-0.575,0.572-1.271,1.02-1.912,1.526c0.082,0.188,0.162,0.374,0.243,0.561
                c0.598,0.057,1.195,0.162,1.793,0.163c19.641,0.007,39.282,0.02,58.922-0.052c0.941-0.003,2.188-0.516,2.773-1.221
                c13.901-16.768,37.278-20.355,55.427-8.435c0.534,0.351,1.087,0.673,1.897,1.171c0.562-13.917,6.121-25.094,17.117-33.316
                c8.273-6.186,17.731-8.827,27.992-7.969c12.292,1.029,22.301,6.557,29.884,16.311c7.618,9.798,9.383,21.164,8.319,33.34
                c18.298,0,36.247,0,54.627,0c-0.717-0.768-1.224-1.341-1.763-1.881c-5.603-5.612-11.237-11.193-16.807-16.839
                c-2.439-2.473-3.238-5.47-2.048-8.788c1.185-3.3,3.653-5.152,7.121-5.547c2.489-0.283,4.742,0.495,6.503,2.25
                c13.019,12.975,26.001,25.987,38.989,38.993c0.097,0.096,0.122,0.26,0.232,0.506c-0.352,0.394-0.716,0.842-1.121,1.248
                c-12.471,12.476-24.963,24.93-37.405,37.434c-2.46,2.472-5.286,3.571-8.655,2.678c-6.261-1.657-8.304-9.279-3.717-13.975
                c5.643-5.78,11.407-11.442,17.116-17.157c0.514-0.514,1.004-1.052,1.724-1.811c-18.304,0-36.319,0-54.483,0
                c0,30.334,0,60.665,0,91.239c13.89-9.197,28.478-11.049,43.389-3.62c14.94,7.443,22.335,20.143,23.337,37.01
                c0.724-0.455,1.377-0.853,2.019-1.271c25.514-16.683,59.668-1.294,64.069,28.897c0.329,2.261,0.438,4.571,0.438,6.858
                c0.008,49.065,0.01,98.131-0.044,147.196c-0.02,17.419-0.828,34.81-2.727,52.131c-1.273,11.625-3.308,23.121-6.253,34.456
                c-0.863,3.327-2.756,5.607-5.669,7.203c-1.927,0-3.854,0-5.781,0c-5.595-4.042-6.211-5.865-4.46-12.769
                c0.727-2.864,1.57-5.726,1.977-8.641c1.665-11.935,3.612-23.854,4.664-35.846c2.215-25.28,1.544-50.653,1.578-75.995
                c0.05-35.801-0.011-71.602,0.027-107.402c0.011-10.125-4.041-17.998-12.879-22.987c-16.664-9.408-36.981,2.72-37.091,22.103
                c-0.106,18.901-0.021,37.803-0.035,56.703c0,1.18,0.034,2.401-0.245,3.532c-0.96,3.895-4.551,6.383-8.663,6.147
                c-3.926-0.224-7.092-3.133-7.636-7.09c-0.15-1.096-0.167-2.215-0.169-3.323c-0.008-29.501-0.014-59.001-0.003-88.501
                c0.003-11.087-4.796-19.33-14.917-23.912c-16.581-7.505-34.974,4.692-35.042,23.149c-0.092,24.681-0.018,49.365-0.031,74.047
                c-0.002,4.687-2.338,7.784-6.541,8.819c-4.357,1.072-9.081-1.898-9.906-6.33c-0.256-1.372-0.265-2.802-0.265-4.204
                c-0.012-73.751-0.035-147.502,0.021-221.252c0.007-8.993-3.013-16.452-10.365-21.711c-8.016-5.733-16.804-6.477-25.625-2.166
                c-9.035,4.417-13.498,12.082-14.008,22.078c-0.049,0.961-0.014,1.926-0.014,2.89c-0.001,73.75,0,147.501-0.002,221.252
                c0,5.21-2.086,8.265-6.375,9.39c-4.469,1.173-9.257-1.782-10.084-6.318c-0.25-1.373-0.259-2.802-0.259-4.205
                c-0.012-62.632-0.039-125.265,0.021-187.897c0.009-8.999-3.04-16.447-10.4-21.693c-8.022-5.719-16.811-6.45-25.627-2.121
                c-9.029,4.433-13.477,12.104-13.971,22.1c-0.047,0.961-0.014,1.927-0.014,2.891c0,81.83,0.001,163.66-0.007,245.49
                c0,1.331,0.08,2.7-0.191,3.987c-0.813,3.859-4.41,6.47-8.522,6.354c-3.892-0.107-7.206-3.015-7.8-6.913
                c-0.167-1.092-0.201-2.213-0.201-3.32c-0.011-21.272,0.161-42.548-0.053-63.818c-0.251-25.021-11.808-43.114-34.218-54.25
                c-1.385-0.688-2.854-1.223-4.316-1.741c-5.745-2.04-11.088,1.557-11.394,7.648c-0.03,0.592-0.009,1.187-0.009,1.779
                c-0.002,48.253-0.002,96.506-0.007,144.759c-0.002,24.977,5.123,48.829,16.524,71.082c4.369,8.526,9.874,16.482,15.055,24.574
                c2.91,4.545,2.068,9.962-2.356,12.523c-0.692,0.4-1.419,0.74-2.129,1.108C127.308,483.51,125.974,483.51,124.64,483.51z" />
             </svg>

            </p>
         <div class="timeline_h overflow-auto">
           
           <ol class="ps-timeline">

             <?php

              $array = array('posts_per_page' => '-1', 'post_type' => 'about_us', 'category_name' => 'Company Progress', 'order' => 'ASC');

              $args = get_posts($array);

              $i = 2;

              foreach ($args as $arg) {

                $arg_slug = $arg->post_name;

                $remainder = $i % 2;

                if ($remainder == 0) {

              ?>

                 <li>

                   <div class="img-handler-top">

                     <img src="<?php echo get_template_directory_uri(); ?>/images/icons/timeline_1.svg" class="img-fluid" />

                     <h4><?php echo $arg->post_title; ?></h4>

                   </div>

                   <div class="ps-bot">

                     <?php echo $arg->post_content; ?>

                   </div>

                   <span class="ps-sp-top"></span>

                 </li>

               <?php } else { ?>

                 <li>

                   <div class="img-handler-bot">

                     <img src="<?php echo get_template_directory_uri(); ?>/images/icons/timeline_2.svg" class="img-fluid" />

                     <h4><?php echo $arg->post_title; ?></h4>

                   </div>

                   <div class="ps-top">

                     <?php echo $arg->post_content; ?>

                   </div>

                   <span class="ps-sp-bot"></span>

                 </li>

             <?php }

                $i++;
              } ?>

           </ol>

         </div>

       </div>

     </div>

   </div>

 </section>

 <?php get_footer(); ?>

 <script>
   $(".ps-timeline").draggable({

     axis: "x",

     containment: '.overflow',

     scroll: false

   });
 </script>