 <?php

  /* Template Name:Portfolio*/



  get_header(); ?>

 <?php

  //Home About section

  $latest_timeline = get_post(158);

  $latest_timeline_title = $latest_timeline->post_title;

  $latest_timeline_content = $latest_timeline->post_content;

  $latest_timeline_img = get_the_post_thumbnail_url($latest_timeline, 'full');

  ?>



 <?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>

 <?php if (!empty(get_the_post_thumbnail())) { ?>



   <section class="page_banner" style="background-image:url(<?php echo $image_url; ?>">

     <div class="page_title mt-lg-5 mt-md-3 pt-5">

       <div class="default_title wow fadeIn">

         <h2><?php echo the_title(); ?></h2>

       </div>

     </div>

   </section>



 <?php } else { ?>

   <section class="page_banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/about_page_banner.jpg">

     <div class="page_title mt-lg-5 mt-md-3 pt-5">

       <div class="default_title wow fadeIn">

         <h2><?php echo the_title(); ?></h2>

       </div>

     </div>

   </section>

 <?php } ?>



 <section class="portfolio_sec1">

   <div class="container">

     <div class="default_title wow fadeIn mt-5 mb-5 text-center">

       <!-- <h2>Timeline</h2> -->

     </div>

     <div class="row">

       <?php

        $args = array('post_type' => 'portfolio_page', 'category_name' => 'timeline', 'posts_per_page' => -1, 'order' => 'ASC');

        $the_query = new WP_Query($args);

        ?>

       <?php if ($the_query->have_posts()) : ?>

         <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

           <div class="col-lg-4 col-md-6">

             <div class="timeline_box d-flex flex-column px-2 mb-4">

               <div class="position-relative">

                 <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">

                 <span><?php echo the_title(); ?></span>

               </div>

               <?php echo the_content(); ?>

             </div>

           </div>

         <?php endwhile;

          wp_reset_postdata(); ?>

       <?php else :  ?>

         <p><?php echo ('Sorry, no posts matched your criteria.'); ?></p>

       <?php endif; ?>

       <div class="col-md-12">

         <div class="timeline_box d-flex flex-column px-2 mb-4">

           <div class="position-relative">

             <img src="<?php echo $latest_timeline_img ?>" class="img-fluid">

             <span><?php echo $latest_timeline_title ?></span>

           </div>

           <?php echo $latest_timeline_content ?>



         </div>

       </div>

     </div>

   </div>

 </section>

 <section class="home_sec_new1 for_portfolio">

   <div class="container-fluid">

     <div class="row">
       
       <div class="col-md-6">

         <section class="charts_orb">

           <div class="row">
           <div class="col-md-12">
         <div class="default_title title_2 pb-4 ">

           <h2>Our Market Focus</h2>

         </div>
       </div>
             <?php

              $args = array('post_type' => 'home_page_post', 'category_name' => 'Company Progress', 'posts_per_page' => 4, 'order' => 'ASC');

              $the_query = new WP_Query($args);

              ?>

             <?php if ($the_query->have_posts()) : ?>

               <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                 <div class="col-md-6">

                   <div class="orb my-lg-4 my-2">

                     <div class="orb_graphic">

                       <?php echo the_content(); ?>

                       <p><?php echo the_title(); ?></p>

                     </div>

                   </div>

                 </div>

               <?php endwhile;

                wp_reset_postdata(); ?>

             <?php else :  ?>

               <p><?php echo ('Sorry, no posts matched your criteria.'); ?></p>

             <?php endif; ?>

           </div>

         </section>

       </div>

       <div class="col-md-6 p-0">

         <img src="<?php echo get_template_directory_uri(); ?>/images/image1.jpg" class="img-fluid">

       </div>

     </div>

   </div>

 </section>

 <?php get_footer(); ?>