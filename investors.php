 <?php

  /* Template Name: investors*/



  get_header(); ?>



 <?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>

 <?php if (!empty(get_the_post_thumbnail())) { ?>


   <section class="page_banner" style="background-image:url(<?php echo $image_url; ?>">

     <div class="page_title mt-lg-5 mt-md-3 pt-5">

       <div class="default_title wow fadeIn">

         <h2>Earn Mailbox Money</h2>

       </div>

     </div>

   </section>

 <?php } else { ?>

   <section class="page_banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/investors_page_banner.jpg">

     <div class="page_title mt-lg-5 mt-md-3 pt-5">

       <div class="default_title wow fadeIn">

         <h2><?php echo the_title(); ?></h2>

       </div>

     </div>

   </section>

 <?php } ?>
 <section class="investors_sec2" id="on_investor_form">

<div class="container">

  <div class="row">

    <div class="col-md-12">

      <div class="login_signup">

        <div class="row  flex-lg-row flex-column-reverse">

          <div class="col-md-12 col-lg-8">

            <!--Forms-->

            <div class="box-2">

              <div class="signup-form-container form_part">

                <div class="default_title title_2 mb-4">

                  <h2>Sign Up to View Opportunities</h2>

                </div>
                <iframe title="sign up" width="100%" height=615px" src="https://mailboxmoney.invportal.com/signup" frameBorder="0"></iframe>

              </div>

              <div class="login-form-container form_part">

                <div class="default_title title_2 mb-4">

                  <h2>Login Account</h2>

                </div>
                <iframe title="login" width="100%" height="615px" src="https://mailboxmoney.invportal.com/login" frameBorder="0"></iframe>

              </div>

            </div>

          </div>

          <div class="col-md-12 col-lg-4">              

            <div class="box-1" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/form_bg.jpg);">

              <div class="content-holder flex-column d-flex">

                <div class=" pb-lg-5 mb-lg-5">

                  <img src="<?php echo get_template_directory_uri(); ?>/images/icons/logo.svg" class="img-fluid">

                </div>

                <div class="mt-lg-5 pt-5 d-flex flex-column">

                  <div class="default_title title_2 mb-3">

                    <h2>Welcome to our community</h2>

                  </div>

                  <p>To access your account, please identify yourself by providing the information requested in the fields below, then click "Login". </p>

                  <div class="mt-4">

                    <button class="button-1 submit_btn" onclick="signup()">Login</button>

                    <button class="button-2 submit_btn" onclick="login()">Sign up</button>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>
 <section class="investors_sec1">

   <div class="container">

     <div class="row">

       <div class="col-md-12">

         <div class="investor_content">

           <?php

            $array = array('posts_per_page' => '-1', 'post_type' => 'investors_page', 'order' => 'ASC');

            $args = get_posts($array);

            $i = 2;



            foreach ($args as $arg) { //echo '<pre>';print_r($arg);

              $arg_image = wp_get_attachment_url(get_post_thumbnail_id($arg->ID));

              $arg_slug = $arg->post_name;

              $remainder = $i % 2;

              if ($remainder == 0) {

            ?>

               <div class="row align-items-center flex-md-row flex-column-reverse">

                 <div class="col-md-6">

                   <div class="investor_text pe-xl-5 pe-lg-4 pe-md-2 mt-5 pt-5 mt-md-0 pt-md-0 wow fadeIn">

                     <div class="default_title title_2 mb-4">



                       <h2><?php echo $arg->post_title; ?></h2>

                     </div>

                     <?php echo $arg->post_content; ?>

                   </div>

                 </div>

                 <div class="col-md-6">

                   <div class="investor_img wow fadeIn">

                     <img src="<?php echo $arg_image; ?>" class="img-fluid" </div>

                   </div>

                 </div>

               </div>

             <?php } else { ?>

               <div class="row align-items-center mt-5 pt-md-5">

                 <div class="col-md-6">

                   <div class="investor_img wow fadeIn">

                     <img src="<?php echo $arg_image; ?>" class="img-fluid">

                   </div>

                 </div>

                 <div class="col-md-6">

                   <div class="investor_text ps-xl-5 ps-lg-4 ps-md-2 mt-5 pt-5 mt-md-0 pt-md-0 wow fadeIn">

                     <div class="default_title title_2 mb-4">

                       <h2><?php echo $arg->post_title; ?></h2>

                     </div>

                     <?php echo $arg->post_content; ?>



                   </div>

                 </div>



               </div>



           <?php }
              $i++;
            } ?>



         </div>

       </div>

     </div>

   </div>

 </section>

 
 <?php get_footer(); ?>

 <script> 

   function signup() {

     document.querySelector(".signup-form-container").style.cssText = "display: none;";

     document.querySelector(".login-form-container").style.cssText = "display: block;";

     //document.querySelector(".container").style.cssText = "background: linear-gradient(to bottom, rgb(56, 189, 149),  rgb(28, 139, 106));";

     document.querySelector(".button-1").style.cssText = "display: none";

     document.querySelector(".button-2").style.cssText = "display: block";



   };



   function login() {

     document.querySelector(".login-form-container").style.cssText = "display: none;";

     document.querySelector(".signup-form-container").style.cssText = "display: block;";

     //document.querySelector(".container").style.cssText = "background: linear-gradient(to bottom, rgb(6, 108, 224),  rgb(14, 48, 122));";

     document.querySelector(".button-2").style.cssText = "display: none";

     document.querySelector(".button-1").style.cssText = "display: block";



   }
 </script>