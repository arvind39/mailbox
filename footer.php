<?php wp_footer();?> 



<footer class="">

    <div class="container">

      <div class="row align-items-center">

        <div class="col-md-12 col-lg-2">

          <div class="footer_logo mb-4 mb-lg-0">

            <a href="<?php echo get_home_url();?>">

              <img src="<?php echo get_template_directory_uri();?>/images/icons/logo.svg" class="img-fluid">

            </a>

          </div>

        </div>

        <div class="col-md-11 col-lg-9">

          <div class="footer_menu">

            <!--<ul>

               <li>

                <a href="index.html">Home</a>

              </li>

              <li>

                <a href="about.html">About</a>

              </li>

              <li>

                <a href="portfolio.html">Portfolio</a>

              </li>

              <li>

                <a href="videos.html">Media</a>

              </li>

              <li>

                <a href="investors.html">Investors</a>

              </li>

              <li>

                <a href="contact.html">Contact</a>

              </li>

            </ul> -->

            <?php 

              /* Primary navigation */

              wp_nav_menu( array(

                'menu' => 'menu1',

                'depth' => 2,

                'container' => false,

                'menu_class' => 'footer_menu',

                //Process nav menu using our custom nav walker

                'walker' => new wp_bootstrap_navwalker())

              );

            ?>

          </div>

        </div>

        <div class="col-md-1 col-lg-1">

          <div class="social d-flex align-items-center">

            <div class="social d-flex align-items-center">

              <a href="https://www.linkedin.com/in/dusten-hendrickson" target="_blank">

                <img src="<?php echo get_template_directory_uri();?>/images/icons/linkedin.svg" class="img-fluid">

              </a>           

            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="copy_right d-flex flex-column justify-content-center">

      <p>Copyright &copy; 2021. All Right Reserved. Powered by <a target="_blank"

          href="http://www.plaxonic.com/">Plaxonic</a></p>

    </div>

</footer>

  





<script src="<?php echo get_template_directory_uri();?>/js/jquery-3.4.0.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/bootstrap.bundle.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/wow.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/owl.carousel.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/lightgallery.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/lg-video.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/video.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/jquery.simplePagination.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/chosen.jquery.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/jquery-ui.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/custom.js"></script>

 

  



  </body>

 

</html>