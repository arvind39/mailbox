<?php

add_action( 'after_setup_theme', 'wpt_setup' );
    if ( ! function_exists( 'wpt_setup' ) ):
        function wpt_setup() {
            register_nav_menu( 'primary', __( 'Primary navigation', 'wptuts' ) );
        } endif;
 function wpt_register_js() {
     //wp_register_script('jquery.bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', 'jquery');
     //wp_enqueue_script('jquery.bootstrap.min');
 }
add_action( 'init', 'wpt_register_js' );

 //For Menu 
require_once('wp_bootstrap_navwalker.php');
 add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active';
     }
     return $classes;
}

//For Feacture Image
add_theme_support( 'post-thumbnails' );

 //Remove all classes and IDs from Nav Menu
 function wp_nav_menu_remove($var) {
    return is_array($var) ? array_intersect($var, array('current-menu-item')) : '';
 }
 add_filter('page_css_class', 'wp_nav_menu_remove', 100, 1);
 add_filter('nav_menu_item_id', 'wp_nav_menu_remove', 100, 1);
 add_filter('nav_menu_css_class', 'wp_nav_menu_remove', 100, 1);




//Add custon attribute in contat form 7

add_filter( 'wpcf7_form_elements', 'imp_wpcf7_form_elements' );
function imp_wpcf7_form_elements( $content ) {
    $str_pos = strpos( $content, 'name="phone_number"' );
    if ( $str_pos !== false ) {
        $content = substr_replace( $content, ' pattern= "\d{10}" title="Only 10 digits mobile number"', $str_pos, 0 );
    }
    return $content;
}

// Remove p tags from category description
remove_filter('term_description','wpautop');

?>
