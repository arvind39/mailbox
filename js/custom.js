

//wow animation



new WOW().init();



/***mobile menu icon****/



$(document).mouseup(function (e) {

  var container = $(".navbar-toggler");

  $('.navbar-toggler').toggleClass('change');

  // If the target of the click isn't the container

  if (!container.is(e.target) && container.has(e.target).length === 0) {

    $('.navbar-toggler').removeClass('change');

    $('.navbar-collapse').collapse('hide');

  }

});



//Banner Slider



$('#banner_slider').owlCarousel({

  loop: true,

  margin: 0,

  nav: false,

  mouseDrag: true,

  autoplay: true,

  autoplayTimeout: 8000,

  animateOut: 'slideOutUp',

  dots: false,

  dotsData: false,

  responsive: {

    0: {

      items: 1

    },

    600: {

      items: 1

    },

    1000: {

      items: 1

    }

  },

});





//Slider number

$(document).ready(function () {

  var owl_move = $('#banner_slider');

  owl_move.owlCarousel();

  // Listen to owl events:

  owl_move.on('changed.owl.carousel', function (event) {

    var s_number = parseInt($(".main_slider .owl-dot.active").text());

    if (s_number == 1) {

      $(".main_slider .progressbar .progress-bar").animate({ width: "33.3333%" }), 0;

    } else if (s_number == 2) {

      $(".main_slider .progressbar .progress-bar").animate({ width: "66.3333%" }), 0;

    } else if (s_number == 3) {

      $(".main_slider .progressbar .progress-bar").animate({ width: "100%" }), 0;



    }

  })

});







//Header Fixed

window.onscroll = function () { myFunction() };

var header = document.getElementById("myHeader");

var sticky = header.offsetTop;

function myFunction() {

  if (window.pageYOffset > sticky) {

    header.classList.add("sticky");

  } else {

    header.classList.remove("sticky");

  }

}



//Equal Height box

$(document).ready(function () {
  if ($(window).width() >= 768) {
    $('.home_sec4').each(function () {

      var highestBox = 0;

      $('.service_box', this).each(function () {

        if ($(this).height() > highestBox) {

          highestBox = $(this).height();

        }

      });

      $('.service_box', this).height(highestBox);

    });
  }


});



$(document).ready(function () {
  if ($(window).width() >= 1024) {
    $('.portfolio_sec1').each(function () {

      var highestBox = 0;

      $('.col-md-4', this).each(function () {

        if ($(this).height() > highestBox) {

          highestBox = $(this).height();

        }

      });

      $('.col-md-4', this).height(highestBox);

    });
  }


});



//Equal Height box

$(document).ready(function () {
  if ($(window).width() >= 1024) {
    $.fn.equalHeights = function () {

      var max_height = 0;

      $(this).each(function () {

        max_height = Math.max($(this).height(), max_height);

      });

      $(this).each(function () {

        $(this).height(max_height);

      });
    }


  };



  $(document).ready(function () {

    $('.video_title').equalHeights();

  });

});



//testimonial Slider

$(document).ready(function () {

  $("#testimonial-slider").owlCarousel({

    items: 1,

    pagination: true,

    autoPlay: true,

    nav: true,

    navText: ['A', 'B'],

    itemsDesktop: [1000, 1],



  });



  $("#testimonial-slider .owl-next").html('<img src="https://amazing7studio.com/mailbox/wp-content/themes/mailboxmoney/images/icons/right-arrow.svg" />');

  $("#testimonial-slider .owl-prev").html('<img src="https://amazing7studio.com/mailbox/wp-content/themes/mailboxmoney/images/icons/left-arrow.svg" />');

});



//Video gallery



jQuery("#country, #invest, #investor ").chosen({ no_results_text: "Oops, nothing found!" });

jQuery('select').chosen({

  allow_single_deselect: true

});




$(document).ready(function () {
  if ($(window).width() < 700) {
    $(".box-1 .submit_btn").click(function () {
      $('html, body').animate({
        scrollTop: $(".box-2").offset().top
      }, 1000);
    });
  };
});
