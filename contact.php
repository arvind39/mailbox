<?php
/* Template Name: Contact*/
get_header(); ?>
<?php
$contact_post = get_post(200);
$post_title = $contact_post->post_title;
$post_content = $contact_post->post_content;
?>
<?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>
<?php if (!empty(get_the_post_thumbnail())) { ?>
  <section class="page_banner" style="background-image:url(<?php echo $image_url; ?>">
    <div class="page_title mt-lg-5 mt-md-3 pt-5">
      <div class="default_title wow fadeIn">
        <h2>Get in Touch
        </h2>
      </div>
    </div>
  </section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/about_page_banner.jpg">
    <div class="page_title mt-lg-5 mt-md-3 pt-5">
      <div class="default_title wow fadeIn">
        <h2>
          <?php echo the_title(); ?>
        </h2>
      </div>
    </div>
  </section>
<?php } ?>
<section class="contact_sec1">
  <div class="container">
    <div class="default_title title_1 text-center mb-4">
      <!-- <h2>Don’t be a stranger.</h2> -->
      <h2>
        <?php echo $post_title ?>
      </h2>
    </div>
    <div class="row">
      <div class="col text-center">
        <div class="contact_box1 wow fadeIn">
          <!-- <?php //echo $post_content 
                ?> -->
                          <!-- <h3 class="fw-bold" style="color:#9ECD54;">Just say hello.</h3>
                <p>Feel free to get in touch with me. I am always open to discussing new projects, creative ideas or
                opportunities to be part of your visions.</p> -->
          <div class="">
            <!-- <p>Need help?
            </p> 
            <a href="mailto:Dusten@mailboxmoneyre.com">
              <h5>Dusten@mailboxmoneyre.com
              </h5>
            </a>-->
          </div>
          <div class="">
            <!-- <p>Feel like talking
            </p> -->
            <a href="mailto:Dusten@mailboxmoneyre.com"  class="link_btn mt-xxl-4 mt-xl-2 mt-2">
              <span>Email Dusten</span>
              <svg width="13px" height="10px" viewBox="0 0 13 10">
                <path d="M1,5 L11,5">
                </path>
                <polyline points="8 1 12 5 8 9">
                </polyline>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div class="col text-center">
        <a href="tel:6056911933"  class="link_btn mt-xxl-4 mt-xl-2 mt-2">
          <span>Schedule a Call</span>
          <svg width="13px" height="10px" viewBox="0 0 13 10">
            <path d="M1,5 L11,5">
            </path>
            <polyline points="8 1 12 5 8 9">
            </polyline>
          </svg>
        </a>
      </div>
      <div class="col text-center">
        <a href="<?php $url = home_url(); echo $url; ?>/investors/"  class="link_btn mt-xxl-4 mt-xl-2 mt-2">
          <span>Join Us</span>
          <svg width="13px" height="10px" viewBox="0 0 13 10">
            <path d="M1,5 L11,5">
            </path>
            <polyline points="8 1 12 5 8 9">
            </polyline>
          </svg>
        </a>
      </div>
      <!-- <div class="col-md-6">
<div class="contact_box2 mt-5 mt-md-0 wow fadeIn">
<h2 class="pb-4">Join Investment Community</h2>
<div class="contact_us">
<iframe title="sign up" width="100%" height="450px" src="https://mailboxmoney.invportal.com/signup" frameBorder="0"></iframe>
<?php //echo do_shortcode('[contact-form-7 id="201" title="Contact form 1"]');
?>
</div>
</div>
</div> -->
    </div>
  </div>
</section>
<?php get_footer(); ?>
<script>
  jQuery(document).ready(function(jQuery) {
    /*
     * for validating particular field use id
     */
    //jQuery("#firstName").prop('required',true);
    /*
     * for validating all the input field 
     */
    jQuery(".wpcf7-form-control").prop('required', true);
    jQuery(".wpcf7-form").removeAttr('novalidate');
  });
</script>