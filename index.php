<?php /* Template Name: indexpage */ 
get_header();	

$catID = get_query_var( 'cat' );
?>
	        
<section class="inner-banner">
	<img src="<?php echo get_template_directory_uri();?>/images/blog/blog-banner.jpg" class="img-fluid" />
	<div class="blog-head">
		<h2 class="heading"><span>Our</span><br />Blog</h2>
	</div>       
</section>

<section class="blog">
	<div class="container-fluid">
        <div class="row justify-content-center">
        	<?php 
             $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 3;
        $args= array('posts_per_page' => '-1',  'orderby' => 'post_date', 'paged' => $paged,'order' => 'ASC', 'post_type' => 'blogs','category__in' => array($catID )); 
        $recent = get_posts($args);
        $count = 0; 
            foreach( $recent as $post ){
               $image_url= wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                $post_date = get_the_date( 'F j, Y' ); 
                 $ds1=date('j',strtotime($post_date)); 
         $ds=date('M',strtotime($post_date)); 
        $ds2=date('Y',strtotime($post_date)); 
                ?>
            <div class="col-lg-11">
            	<div class="row">                	
                    <div class="col-lg-11">
                    	<div class="blog-img">
                            <img src="<?php echo $image_url;?>" class="img-fluid" />
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-lg-8">
						<div class="blog-date">
                            <h3><?php echo  $ds1;?></h3>
                            <h4><?php echo  $ds;?></h4>
                        </div>
                    	<div class="blog-content-box">
                        	<h4><?php echo $post->post_title;?></h4>
                            <p><?php echo substr($post->post_content,0,300);?>..</p>
                            <div class="d-flex justify-content-start">
                            	<a href="<?php echo  get_permalink($post->ID);?>" class="view-all-btn">READ MORE</a>
                            </div>                            
                        </div>
                    </div>                    
                </div>
            </div>
            <?php  
             
            }
          
            ?>
                                                            	
        </div>
        <!-- <div class="row justify-content-end">
                	<div class="col-lg-4">
                    	<div class="pagination">
                        	<ul>
                            	<li><a href="javscript:void(0)">PREV.</a></li>
                                <li class="active"><a href="javscript:void(0)">1</a></li>
                                <li><a href="javscript:void(0)">2</a></li>
                                <li><a href="javscript:void(0)">3</a></li>
                                <li><a href="javscript:void(0)">4</a></li>
                                <li><a href="javscript:void(0)">NEXT</a></li>
                            </ul>
                        </div>
                    </div>
                </div> -->
	</div>
</section>
<?php get_footer();?>

