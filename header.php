<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="user-scalable=no, width=device-width" />
  <title>MailBox Money</title>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/images/icons/fav-icon.ico" /> 

  <link href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri();?>/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri();?>/css/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri();?>/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri();?>/css/lightgallery.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri();?>/css/video-js.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo get_template_directory_uri();?>/css/chosen.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri();?>/css/main.css" rel="stylesheet" type="text/css" />
    <?php wp_head();?>
   </head>
   <body>
  <header class="w-100">
    <div class="header_top py-3 mb-sm-3 mb-0">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="head_1 d-flex align-items-center justify-content-between">
              <a href="https://calendly.com/mailboxmoneyre" target="_blank" class="tel">
                Let’s talk about investment
              </a>
              <a href="mailto:dusten@mailboxmoneyre.com">
                <img src="<?php echo get_template_directory_uri();?>/images/icons/mail.svg" class="img-fluid me-2">
                dusten@mailboxmoneyre.com
              </a>
            </div>
          </div>
        </div>
      </div>

    </div>
    <div class="main_menu py-2" id="myHeader">
      <div class="container">
        <div class="row">

          <div class="col-md-12">
            <div class="head_2">
              <nav class="navbar navbar-expand-lg p-0">
                <div class="d-flex w-100 d-flex align-items-center justify-content-between">
                  <a class="navbar-brand" href="<?php echo get_home_url();?>">
                    <img src="<?php echo get_template_directory_uri();?>/images/icons/logo.svg" class="img-fluid">
                  </a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_menu"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                  </button>
                  <div class="collapse navbar-collapse justify-content-end" id="main_menu">
                  <a class="navbar-brand d-lg-none d-block d-flex mb-4" href="index.html">
                      <img src="<?php echo get_template_directory_uri();?>/images/icons/logo.svg" class="img-fluid">
                    </a>
                    <!-- <ul class="navbar-nav mb-2 mb-lg-0">
                      <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.html">Home</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="investors.html">Investors</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="about.html">About</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Portfolio</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Videos</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Contact</a>
                      </li>

                    </ul> -->
                    <?php 
      /* Primary navigation */
          wp_nav_menu( array(
         'menu' => 'menu1',
          'depth' => 2,
         'container' => false,
         'menu_class' => 'navbar-nav',
          //Process nav menu using our custom nav walker
          'walker' => new wp_bootstrap_navwalker())
         );
      ?>      

                  </div>
                </div>
              </nav>
            </div>

          </div>
        </div>
      </div>
    </div>
  </header>