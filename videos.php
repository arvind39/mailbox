<?php
/* Template Name: Videos*/
get_header(); ?>
<?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>
<?php if (!empty(get_the_post_thumbnail())) { ?>
  <section class="page_banner" style="background-image:url(<?php echo $image_url; ?>">
    <div class="page_title mt-5 pt-5">
      <div class="default_title wow fadeIn">
        <h2>
          <?php echo the_title(); ?>
        </h2>
      </div>
    </div>
  </section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/about_page_banner.jpg">
    <div class="page_title mt-lg-5 mt-md-3 pt-5">
      <div class="default_title wow fadeIn">
        <h2>
          <?php echo the_title(); ?>
        </h2>
      </div>
    </div>
  </section>
<?php } ?>
<section class="video_sec1">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="video_list">
          <?php
          $args = array('post_type' => 'videos_page',  'posts_per_page' => -1, 'order' => 'ASC');
          $count = 1;
          $the_query = new WP_Query($args); ?>
          <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
              <div class="video_box" style="display:none;" id="video<?php echo $count ?>">
                <video class="lg-video-object lg-html5 video-js vjs-default-skin" poster='<?php echo esc_url(get_field('video_poster')['url']); ?>' controls="" preload="none">
                  <source src="<?php echo esc_url(get_field('video_link')['url']); ?>" type="video/mp4">
                  Your browser does not support HTML5 video.
                </video>
              </div>
              <?php $count++ ?>
            <?php endwhile;
            wp_reset_postdata(); ?>
          <?php else :  ?>
            <p>
              <?php echo ('Sorry, no posts matched your criteria.'); ?>
            </p>
          <?php endif; ?>
        </div>
        <div class="cont">
          <div class="d-flex align-items-center justify-content-between flex-md-row flex-column wow fadeIn">
            <div class="default_title title_2 mb-4">
              <h2>Walk and Talk Series</h2>
              <p>Our founder Dusten Hendrickson and the rest of the team are always on the move, Dusten loves to bring you behind the scenes and provide value about what investing in multifamily real estate is really like. Watch any of our media and Walk and Talk videos to see us visiting properties, people, and deals we’re working with.</p>
            </div>

          </div>
          <div class="text-end" id="custom_pagination"></div>
          <div class="row" id="lightgallery">
            <?php
            $args = array('post_type' => 'videos_page',  'posts_per_page' => -1, 'order' => 'ASC');
            $count = 1;
            $the_query = new WP_Query($args); ?>
            <?php if ($the_query->have_posts()) : ?>
              <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <div class="col-lg-4 col-md-6 video" data-html="#video<?php echo $count ?>">
                  <div class="gallery_box mt-5">
                    <a href="" class="">
                      <img class="img-fluid" src="<?php echo esc_url(get_field('video_poster')['url']); ?>">
                      <div class="play_btn">
                        <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M54.628 9.37203C42.1319 -3.12401 21.8681 -3.12401 9.37203 9.37203C-3.12401 21.8681 -3.12401 42.1319 9.37203 54.628C21.8681 67.124 42.1319 67.124 54.628 54.628C67.124 42.1319 67.124 21.8681 54.628 9.37203Z" fill="#9ECD54" />
                          <path d="M45.9823 33.9588L35.9855 39.7003L26.1913 45.3741C25.8535 45.6443 25.3807 45.7794 24.9079 45.7794C23.6245 45.7794 22.6113 44.7662 22.6113 43.4828V32V20.4496C22.6113 20.0443 22.6789 19.7066 22.8815 19.3013C23.4894 18.2206 24.9079 17.8153 25.9886 18.4908L35.8504 24.1647C35.9179 24.1647 35.9179 24.2322 35.9855 24.2322L45.9823 29.9736C46.32 30.1763 46.5902 30.4464 46.8604 30.7842C47.4683 31.9325 47.063 33.3509 45.9823 33.9588Z" fill="white" />
                        </svg>
                      </div>
                    </a>
                    <div class="video_title p-3">
                      <p>
                        <?php the_title(); ?>
                      </p>
                    </div>
                  </div>
                </div>
                <?php $count++ ?>
              <?php endwhile;
              wp_reset_postdata(); ?>
            <?php else :  ?>
              <p>
                <?php echo ('Sorry, no posts matched your criteria.'); ?>
              </p>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
<script>
  lightGallery(document.getElementById('lightgallery'), {
    videojs: true,
    videoMaxWidth: '70%'
  })
</script>
<script>
  var items = $("#lightgallery .video");
  var numItems = items.length;
  var perPage = 9;
  items.slice(perPage).hide();
  $('#custom_pagination').pagination({
    items: numItems,
    itemsOnPage: perPage,
    prevText: "&laquo;",
    nextText: "&raquo;",
    onPageClick: function(pageNumber) {
      var showFrom = perPage * (pageNumber - 1);
      var showTo = showFrom + perPage;
      items.hide().slice(showFrom, showTo).show();
    }
  });
</script>