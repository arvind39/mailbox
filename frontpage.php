 <?php

  /* Template Name: Front Page*/



  get_header(); ?>



 <?php



  //Home Slider Video

  $banner_video = get_post(279);

  $banner_video_link = $banner_video->post_content;

  $banner_video_poster = get_the_post_thumbnail_url($banner_video, 'thumbnail');



  //Home About section

  $home_about = get_post(57);

  $home_about_title = $home_about->post_title;

  $home_about_content = $home_about->post_content;

  $home_about_img = get_the_post_thumbnail_url($home_about, 'thumbnail');



  //Home section 2

  $market_focus = get_post(61);

  $market_focus_title = $market_focus->post_title;

  $market_focus_content = $market_focus->post_content;

  $market_focus_img = get_the_post_thumbnail_url($market_focus, 'thumbnail');







  //Home section 4

  $potential = get_post(88);

  $home_potential_title = $potential->post_title;

  $home_potential_content = $potential->post_content;



  //Home section video

  $video = get_post(107);

  $home_video_content = $video->post_content;



  //Home service

  $service = get_post(283);

  $about_service_title = $service->post_title;

  $about_service = $service->post_content;



  ?>



 <!-- <section class="home_sec1">

   <div class="main_slider">

     <div class="owl-carousel owl-theme" id="banner_slider">

       <?php

        $args = array('post_type' => 'home_page_slider', 'posts_per_page' => 3);

        $the_query = new WP_Query($args);

        ?>

       <?php if ($the_query->have_posts()) : ?>

         <?php $count = 1; ?>

         <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

           <div class="item" data-dot="<button>0<?php echo $count ?></button>">

             <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="images not found">

             <div class="cover">

               <div class="container">

                 <div class="row">

                   <div class="col-md-8">

                     <div class="header-content">

                       <h4><?php the_title(); ?></h4>

                       <?php the_content(); ?>

                       <?php if (get_field('slider_button_link')) : ?>

                         <a href="http://localhost/mailboxmoney/about/" class="link_btn mt-4">

                           <span>View Details</span>

                           <svg width="13px" height="10px" viewBox="0 0 13 10">

                             <path d="M1,5 L11,5"></path>

                             <polyline points="8 1 12 5 8 9"></polyline>

                           </svg>

                         </a>

                       <?php endif; ?>

                     </div>

                   </div>

                   <div class="col-md-4">

                   </div>

                 </div>



               </div>

             </div>

           </div>

           <?php $count++; ?>

         <?php endwhile;

          wp_reset_postdata(); ?>

       <?php else :  ?>

         <p><?php echo ('Sorry, no posts matched your criteria.'); ?></p>

       <?php endif; ?>



     </div>

     <div class="custom_prograss_bar">

       <div class="progressbar" role="progressbar" style="width: 33.3333%; background: #8ed61d; height: 2px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">

       </div>



     </div>

 </section> -->



 <section class="home_sec1">

   <video id="banner-video" autoplay='true'>

     <source src="<?php echo get_template_directory_uri(); ?>/./video/Mailbox-Money-Website-Video.mp4" type="video/mp4">

   </video>

   <div class="banner_button">

     <a href="<?php $url = home_url(); echo $url; ?>/investors/" class="link_btn btn_light mt-4">

       <span>Access the Investor Portal</span>

       <svg width="13px" height="10px" viewBox="0 0 13 10">

         <path d="M1,5 L11,5"></path>

         <polyline points="8 1 12 5 8 9"></polyline>

       </svg>

     </a>

   </div>

   </div>

 </section>

 <section class="home_sec2 py-4 py-lg-5 my-5">

   <div class="container">



     <div class="row">



       <div class="col-lg-7 col-md-12">



         <div class="home_about pe-xl-5 pe-lg-2 wow fadeIn">

           <div class="about_title pb-4 pb-md-5 wow fadeIn">

             <h2><?php echo $home_about_title ?></h2>

           </div>

           <?php echo $home_about_content ?>

           <a href="<?php $url = home_url();
                    echo $url; ?>/about/" class="link_btn btn_light mt-4">

             <span>Learn how</span>

             <svg width="13px" height="10px" viewBox="0 0 13 10">

               <path d="M1,5 L11,5"></path>

               <polyline points="8 1 12 5 8 9"></polyline>

             </svg>

           </a>

         </div>

       </div>

       <div class="col-lg-5 col-md-12">

         <div class="home_about_img mt-lg-0 mt-5">

           <!-- <img src="<?php //echo $home_about_img 
                          ?>" class="img-fluid"> -->
           <img src="https://amazing7studio.com/mailbox/wp-content/uploads/2021/05/home_about-1.jpg" class="img-fluid">



         </div>

       </div>

     </div>

   </div>

 </section>

 <section class="home_sec3">

   <div class="container">

     <div class="row">

       <div class="col-lg-10 col-md-12 md-xl-9">

         <div class="box_style1">

           <h3><?php echo $market_focus_title ?></h3>

           <?php echo $market_focus_content ?>

         </div>

       </div>



     </div>

   </div>

 </section>

 <section class="home_sec4">

   <div class="market_focus">

     <img src="<?php echo $market_focus_img ?>" class="img-fluid">

   </div>

   <div class="container">

     <div class="row">

       <div class="col-md-12">

         <div class="service_part pt-4">

           <div class="before_service pb-4">

             <h2 class="fw-bold"><?php echo $about_service_title ?></h2>

             <?php echo $about_service ?>

           </div>

           <div class="row">

             <?php

              $args = array('post_type' => 'home_services', 'posts_per_page' => 4, 'order' => 'ASC');

              $the_query = new WP_Query($args);

              ?>

             <?php if ($the_query->have_posts()) : ?>



               <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                 <div class="col-lg-6 col-md-6 mb-6 mb-lg-0 wow fadeIn">

                   <div class="service_box px-xl-4 px-md-4 py-xl-4 py-md-4 py-3 px-3 mb-4">

                     <h3>

                       <?php echo the_title(); ?>

                     </h3>

                     <p>

                       <?php echo the_content(); ?>

                     </p>



                   </div>

                 </div>

               <?php endwhile;

                wp_reset_postdata(); ?>

             <?php else :  ?>

               <p><?php echo ('Sorry, no posts matched your criteria.'); ?></p>

             <?php endif; ?>

             <div>

             </div>

           </div>

         </div>

       </div>

 </section>



 <section class="home_sec_new2">

   <div class="container">

     <div class="new_title pb-4">

       <h2>View our working deals</h2>

     </div>

     <div class="row">



       <?php

        $args = array('post_type' => 'home_page_post', 'category_name' => 'Market Focus', 'posts_per_page' => 12, 'order' => 'ASC');

        $the_query = new WP_Query($args);

        ?>

       <?php if ($the_query->have_posts()) : ?>



         <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>







           <div class="col-md-6">

             <div class="prograss_box">

               <div class="custom_progress mb-2">

                 <div class="progress">

                   <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?php echo get_field("marke_focus"); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo get_field("marke_focus"); ?>%"></div>

                 </div>

                 <p><?php echo the_title(); ?></p>





               </div>

             </div>

           </div>

         <?php endwhile;

          wp_reset_postdata(); ?>

       <?php else :  ?>

         <p><?php echo ('Sorry, no posts matched your criteria.'); ?></p>

       <?php endif; ?>

     </div>





   </div>

 </section>

 <section class="home_sec5">

   <div class="portfolio">

     <div class="portfolio_video">



       <video id="home_video">

         <source src="<?php echo get_template_directory_uri(); ?>/./video/Mailbox-Money-Website-Video.mp4" type="video/mp4" />

       </video>

     </div>

     <div class="portfolio_text wow fadeIn">

       <div class="default_title title_2 pb-xxl-3">

         <h2><?php echo $home_potential_title ?></h2>

       </div>

       <p><?php echo $home_potential_content ?></p>



       <a href="mailto:dusten@mailboxmoneyre.com" class="link_btn mt-xxl-4 mt-2">

         <span>Say Hello</span>

         <svg width="13px" height="10px" viewBox="0 0 13 10">

           <path d="M1,5 L11,5"></path>

           <polyline points="8 1 12 5 8 9"></polyline>

         </svg>

       </a>



     </div>

   </div>



 </section>

 <section class="home_sec6">

   <div class="container">

     <div class="row">

       <div class="col-md-6">

         <div class="clients pe-xl-2 pe-0 pe-xxl-5 me-0 me-sm-4 me-xxl-5 mt-5 mt-sm-0 pt-md-0 pt-sm-0">

           <div class="default_title title_2 pb-5">

             <h2>Our Clients</h2>

           </div>

           <div class="row">

             <?php

              $args = array('post_type' => 'our_clients', 'posts_per_page' => 6, 'order' => 'ASC');

              $the_query = new WP_Query($args);

              ?>



             <?php if ($the_query->have_posts()) : ?>



               <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>



                 <div class="col-md-6">

                   <div class="logo_box p-3 d-flex align-items-center justify-content-center mb-4">

                     <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">

                   </div>

                 </div>

               <?php endwhile;

                wp_reset_postdata(); ?>

             <?php else :  ?>

               <p><?php echo ('Sorry, no posts matched your criteria.'); ?></p>

             <?php endif; ?>



           </div>

         </div>



       </div>

       <div class="col-md-6">

         <div class="testimonial ps-xl-5 ps-0">

           <div class="default_title title_2">

             <h2>What They Say</h2>

           </div>

           <div class="quote">



             <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 191.029 191.029" style="enable-background:new 0 0 191.029 191.029;" xml:space="preserve">

               <path style="fill:#f1f1f1;" d="M44.33,88.474v15.377h38.417v82.745H0v-82.745h0.002V88.474c0-31.225,8.984-54.411,26.704-68.918

                  C38.964,9.521,54.48,4.433,72.824,4.433v44.326C62.866,48.759,44.33,48.759,44.33,88.474z M181.107,48.759V4.433

                  c-18.343,0-33.859,5.088-46.117,15.123c-17.72,14.507-26.705,37.694-26.705,68.918v15.377h0v82.745h82.744v-82.745h-38.417V88.474

                  C152.613,48.759,171.149,48.759,181.107,48.759z" />

               <g>

               </g>

             </svg>

           </div>

           <div id="testimonial-slider" class="owl-carousel mt-sm-5 mt-md-3 mt-lg-4">





             <?php

              $args = array('post_type' => 'testimonial', 'posts_per_page' => 5, 'order' => 'ASC');

              $the_query = new WP_Query($args);

              ?>



             <?php if ($the_query->have_posts()) : ?>



               <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>



                 <div class="testimonial">

                   <div class="description">

                     <?php the_content(); ?>

                   </div>

                   <div class="testimonial-review mb-4">



                     <h3 class="testimonial-title"><?php the_title(); ?></h3>



                   </div>

                 </div>

               <?php endwhile;

                wp_reset_postdata(); ?>

             <?php else :  ?>

               <p><?php echo ('Sorry, no posts matched your criteria.'); ?></p>

             <?php endif; ?>





           </div>

         </div>



       </div>



     </div>

   </div>

 </section>



 <?php get_footer(); ?>



 <script>
   $("#banner_slider .owl-next").html('<img src="<?php echo get_template_directory_uri(); ?>/images/icons/next.svg" />');

   $("#banner_slider .owl-prev").html('<img src="<?php echo get_template_directory_uri(); ?>/images/icons/previous.svg" />');
 </script>



 <script>
   var myFP = fluidPlayer(

     'home_video', {

       "layoutControls": {

         "controlBar": {

           "autoHideTimeout": 3,

           "animated": true,

           "autoHide": false

         },

         "htmlOnPauseBlock": {

           "html": null,

           "height": null,

           "width": null

         },

         "autoPlay": false,

         "mute": false,

         "allowTheatre": true,

         "playPauseAnimation": true,

         "playbackRateEnabled": false,

         "allowDownload": false,

         "playButtonShowing": true,

         "fillToContainer": true,

         "primaryColor": "#8ed61d",

         "posterImage": "<?php echo get_template_directory_uri(); ?>/./video/video_banner.jpg"

       },



     });
 </script>

 <script>
   var myFP = fluidPlayer(
       'banner-video', {
         "layoutControls": {
           "controlBar": {
             "autoHideTimeout": 3,
             "animated": true,
             "autoHide": true
           },
           "htmlOnPauseBlock": {
             "html": null,
             "height": null,
             "width": null
           },
           "autoPlay": true,
           "mute": true,
           "allowTheatre": false,
           "playPauseAnimation": false,
           "playbackRateEnabled": false,
           "allowDownload": false,
           "playButtonShowing": true,
           "fillToContainer": true,
           "primaryColor": "#8ed61d",
           "posterImage": "<?php echo get_template_directory_uri(); ?>/./video/poster.jpg"
         },
         "vastOptions": {
           "adList": [],
           "adCTAText": false,
           "adCTATextPosition": ""
         }
       });
 </script>

 <script>
   jQuery('.orb_graphic h2').each(function() {

    jQuery(this).prop('Counter', 0).animate({

       Counter: jQuery(this).text()

     }, {

       duration: 1500,

       easing: 'linear',

       step: function(now) {

        jQuery(this).text(Math.ceil(now));

       }

     });

   });
 </script>



 <style>
   section.home_sec1 .fluid_video_wrapper {

     width: 100% !important;

     height: auto !important;

   }
 </style>